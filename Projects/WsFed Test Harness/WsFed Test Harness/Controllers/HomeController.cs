﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

using Microsoft.Owin.Security;
using Microsoft.Owin.Security.WsFederation;

namespace WsFed_Test_Harness.Areas.WsFed.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.ClaimsIdentity = Thread.CurrentPrincipal.Identity;
            return View();  
        }

        [Authorize]
        public ActionResult Login()
        {
            ViewBag.ClaimsIdentity = Thread.CurrentPrincipal.Identity;
            return View();
        }   

        public ActionResult Logout()
        {
            var context = Request.GetOwinContext();
            var response = context.Response;

            //var properties = new AuthenticationProperties();
            //properties.RedirectUri = "https://profile-ppe.livingitup.org.uk/account/logout?returnUrl=http://liuppeauthtest.azurewebsites.net/";
            //context.Authentication.SignOut(properties, WsFederationAuthenticationDefaults.AuthenticationType);

            context.Authentication.SignOut();
            return View();
        }
    }
}