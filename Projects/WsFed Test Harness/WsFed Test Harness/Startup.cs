﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WsFed_Test_Harness.Startup))]

namespace WsFed_Test_Harness
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
