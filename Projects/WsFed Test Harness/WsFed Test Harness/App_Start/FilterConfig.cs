﻿using System.Web;
using System.Web.Mvc;

namespace WsFed_Test_Harness
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
