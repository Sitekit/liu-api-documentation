﻿namespace WsFed_Test_Harness
{
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.WsFederation;
    using Owin;
    using System.Configuration;
    using System.IdentityModel.Tokens;

    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            var metaAddress = ConfigurationManager.AppSettings["metaAddress"];
            var realm = ConfigurationManager.AppSettings["realm"];

            app.SetDefaultSignInAsAuthenticationType(WsFederationAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = WsFederationAuthenticationDefaults.AuthenticationType
            });

            app.UseWsFederationAuthentication(new WsFederationAuthenticationOptions
            {
                MetadataAddress = metaAddress,
                Wtrealm = realm,

                TokenValidationParameters = new TokenValidationParameters
                {
                    ValidAudience = realm                    
                },
            });
        }
    }
}