﻿using System.Web.Mvc;

namespace WsFed_Test_Harness.Areas.WsFed
{
    public class WsFedAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WsFed";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WsFed_default",
                "WsFed/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}