﻿using System.Web.Mvc;

namespace WsFed_Test_Harness.Areas.OAuth2
{
    public class OAuth2AreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "OAuth2";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "OAuth2_default",
                "OAuth2/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}